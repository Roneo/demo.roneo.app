---
title: "Hugo: Test pages"
description: ""
keywords: ""
date: 2020-08-28T04:21:43Z
meta: false
url: test

---


<div class="boxInfo">
This page is a Draft, for testing purpose only
</div>

## Pages liées

- [Markdown](/markdown)
- [shortcodes](/test/shortcodes), galleries, vidéos
- [characters](/test/characters), smiley
- [notices](/test/notices), infoboxes