---
title: "Audio player Crash test"
subtitle: "Let's embed audio files from Archive.org"
description: ""
keywords: "hugo, reference, tutorial, audio player, archive.org"
date: 2020-08-27T04:21:43Z
draft: false
type: markdown
meta: false
url: audio

---

<!--more-->

## Testing Archive.org audio player

<iframe src="https://archive.org/embed/test-record-upload" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

###  Embed audio with clickable "playlist":

<iframe src="https://archive.org/embed/test-record-upload&playlist=1" width="500" height="300" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>


###  Embed audio with clickable "playlist" and specific list height:

<iframe src="https://archive.org/embed/test-record-upload&playlist=1&list_height=150" width="500" height="300" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>


### Archive.org player with multiple files and a cover image

<iframe src="https://archive.org/embed/another-test" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>


### Idem mais avec " Embed audio with clickable "playlist" and specific list height"


<iframe src="https://archive.org/embed/another-test&playlist=1&list_height=150" width="500" height="800" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>


### Idem without specified height

<iframe src="https://archive.org/embed/another-test&playlist=1&list_height=150" width="500" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>


## Loading Archive.org file with HTML <audio>

<figure>
    <figcaption>Listen to the T-Rex:</figcaption>
    <audio
        controls
        src="https://archive.org/download/test-record-upload/SoundRecord-2022-01-09-20-06-25.wav">
            Your browser does not support the
            <code>audio</code> element.
    </audio>
</figure>


## References

https://archive.org/help/audio.php?identifier=another-test
- https://archive.org/download/another-test
- 

