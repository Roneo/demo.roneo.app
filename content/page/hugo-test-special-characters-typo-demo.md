---
title: "Hugo, Smiley and special Characters: a test page"
subtitle: ""
description: ""
keywords: ""
date: 2020-08-25T04:21:43Z
meta: false
url: test/characters

---

<div class="boxInfo">
This page is a Draft, for testing purpose only
</div>

## Pages liées

- [shortcodes](/test/shortcodes), galleries, vidéos
- [characters](/test/characters), smiley
- [notices](/test/notices), infoboxes


Ctrl + majuscule + u+ 2014 = —
Ctrl + majuscule + u + 153 = œ
U+1E98 	ẘ

### Modifier la couleur

👦🏾 = \u{1F466}\u{1F3FE}
Voir https://unicode.org/emoji/charts/full-emoji-modifiers.html

![illustration: unicode-combination](/illustrations/hugo-test-special-characters-typo-demo/unicode-combination.png)

U+2615 ☕ café
26A1 ⚡

Liste 
https://github.com/jagracey/Awesome-Unicode#unicode-blocks
https://en.wikipedia.org/wiki/Miscellaneous_Symbols

ß
Ḹ
ẘ

### Smiley

[from emojipedia](https://emojipedia.org)

◌

😇 :tent:
Leaf 🍃 Bund ! N'aura pas le même rendu partout https://emojipedia.org/leaf-fluttering-in-wind/
Branch 🌿 Bundle
:joy:

<div class="boxInfo"> <strong>About Emoji</strong>

You can use this [list of emoji shortcodes](https://gist.github.com/rxaviers/7360908), but keep in mind that emoji shortcodes vary from application to application. Refer to your Markdown application's documentation for more information.
</div>

----



## HTML

### Colorize the text

`<font color=red> Danger  </font>` → <font color=red> Danger  </font>



### Typo glyphes and symbols

Unicode ? : ☕️




## Special characters

&nbsp; &amp; &copy; &AElig; &Dcaron;
&frac34; &HilbertSpace; &DifferentialD;
&ClockwiseContourIntegral; &ngE;

ᨖ ₿ ⛑



## Emoticon / emoji

Histoire : [Emoticon - Wikipedia](https://en.wikipedia.org/wiki/Emoticon)
  https://blog.emojipedia.org/vaccine-emoji-comes-to-life/

Longue liste et les rendus selon les machines : https://www.unicode.org/emoji/charts/full-emoji-list.html
autre liste : https://unicode.org/emoji/charts/emoji-list.html

Rechercher: https://emojipedia.org/

Who Controls Emoji? (Hint, we want to give the power to you!) http://www.emojination.org/


Différents types : https://en.wikipedia.org/wiki/Emoticon#See_also

https://en.wikipedia.org/wiki/List_of_emoticons

[emojitracker: realtime emoji use on twitter](http://emojitracker.com/)