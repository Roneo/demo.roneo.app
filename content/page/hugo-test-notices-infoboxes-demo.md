---
title: "Hugo: Notices demonstration"
description: ""
keywords: ""
date: 2020-10-02T04:21:43Z
meta: false
url: test/notices

---


<div class="boxInfo">
This page is a Draft, for testing purpose only
</div>

## Pages liées

- [shortcodes](/test/shortcodes), galleries, vidéos
- [characters](/test/characters), smiley
- [notices](/test/notices), infoboxes


## Notices with the module 'hugo-notice'

Requires https://github.com/martignoni/hugo-notice

{{< notice warning >}}
This is a warning notice. Be warned!
{{< /notice >}}

{{< notice note >}}
This is a warning notice. Be warned!
{{< /notice >}}

{{< notice tip >}}
This is a warning notice. Be warned!
{{< /notice >}}

{{< notice info >}}
This is a warning notice. Be warned!
{{< /notice >}}

**Syntax**

```
	{{</* notice warning */>}}
	This is a warning notice. Be warned!
	{{</* /notice */>}}
	 (can be warning, note, tip or info)
```

### Custom info boxes


<div class="boxBell">Disclaimer</div>
<div class="boxCheck">Check</div>
<div class="boxComment">Comment</div>
<div class="boxHeart">Heart</div>
<div class="boxInfo">Info</div>
Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio iusto repellendus sit magnam. Omnis vitae dolores delectus, vero velit ipsum. Quasi libero nam explicabo tenetur doloribus fuga cupiditate quaerat sapiente?
<div class="boxPlus">Plus</div>
<div class="boxStar">Star</div>
<div class="boxWarning">
Warning Lorem ipsum dolor sit, amet consectetur adipisicing elit.

Quae, amet iste! Vitae veritatis earum, exercitationem dolorum inventore molestiae eveniet corporis, neque nulla modi nobis quibusdam quisquam ducimus est at numquam. </div>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta nulla nisi aliquid doloremque, neque distinctio harum, voluptate sequi fuga deleniti voluptas, temporibus enim provident illo ipsum laborum ducimus aut est!

See [this dedicated article](/en/hugo-custom-warning-infoboxes/)

