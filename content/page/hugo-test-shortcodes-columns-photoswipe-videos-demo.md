---
title: "Hugo: Shortcodes demonstration"
subtitle: ""
description: ""
keywords: ""
date: 2020-08-28T04:21:43Z
draft: false
meta: false
url: test/shortcodes

---


<div class="boxInfo">
This page is a Draft, for testing purpose only
</div>

## Pages liées

- [shortcodes](/test/shortcodes), vidéos
- [characters](/test/characters), smiley
- [notices](/test/notices), infoboxes


## Insert videos

```
<video controls loop muted>
<source src="/illustrations/webm.webm" type="video/webm">
Sorry, your browser doesn't support embedded videos.
</video>
```

<video controls loop muted>
<source src="/illustrations/webm.webm" type="video/webm">
Sorry, your browser doesn't support embedded videos.
</video>

### Video from Asciinema

<script id="asciicast-7A9xx0UN6K4oaceojxu979uCy" src="https://asciinema.org/a/7A9xx0UN6K4oaceojxu979uCy.js" async></script>

## Shortcodes

(the support of Shortcodes depends on the theme you use)

### Insert a picture with caption

`{{</* figure src="/img/banner.jpg" title="Landscape" */>}} `

{{< figure src="/img/banner.jpg" title="Landscape" >}}

### Insert a Gist

`{{</* gist spf13 7896402 */>}}`

{{< gist spf13 7896402 >}}

### Video from Youtube

`{{</* youtube w7Ft2ymGmfc */>}}`


{{< youtube w7Ft2ymGmfc >}}

### Param from Front Matter

`{{</* param title */>}}` will display the title : {{< param title >}}

