---
title: "Hugo: fetch data from Github API with a Shortcode"
date: 2022-08-18T16:31:16+00:00
tags:
  - linux
  - hugo
  - githup
  - api
  - shortcode
showtoc: false
weight: 90
draft: true

---

<!--more-->

This is only a demo. See [this post](https://roneo.org/en/hugo-fetch-remote-data-github-api-shortcode/) for an explanation.

## The last 3 starred repo

{{< github-api url="https://api.github.com/users/RoneoOrg/starred?per_page=3" >}}



## The last 5 updates

{{< github-api url="https://api.github.com/users/RoneoOrg/repos?type=owner&sort=updated&per_page=5" >}}

