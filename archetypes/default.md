---
title: "{{ replace .Name "-" " " | title }}"
linkTitle: ""
subtitle: ""
summary: ""
date: {{ .Date }}
description: ""
keywords: ""
draft: true
tags:
    - 
url:
cover:
    image: "<image path/url>"
    # can also paste direct link from external site
    # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
    alt: "<alt text>"
    caption: "<text>"
    relative: false # To use relative path for cover image, used in hugo Page-bundles
---


<!--more-->